(function () {
	'use strict'

	if (supportsRegisterElement()) {
		var dom = document
		var btn = dom.querySelector('input[type="submit"]')
		
		dom.registerElement('todo-list', {
			prototype: Object.create(HTMLUListElement.prototype),
			extends: 'ul'
		})

		dom.registerElement('todo-task', {
			prototype: Object.create(HTMLLIElement.prototype),
			extends: 'li'
		})

		var todo = dom.createElement('todo-list')

		dom.querySelector('body').appendChild(todo)
		btn.addEventListener('click', handleClick)	
	}

	function supportsRegisterElement () {
		return 'registerElement' in document
	}

	function handleClick (evt) {
		evt.preventDefault()

		var input = dom.querySelector('input[type="text"]')
		var text = input.value.trim()

		if (text != '') {
			var temp = dom.createElement('todo-task')

			temp.textContent = text

			dom.querySelector('todo-list').appendChild(temp)

			input.value = ''
		}
	}
})()